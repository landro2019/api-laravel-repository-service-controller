<?php
namespace App\Interfaces;

/**
     * Fofana Lamagnigue
     * 10/09/2022
	 */
interface UserRepositoryImpl{

    public function findAll();
    public function find(int $id);
    public function  save($user);
    public function  update($user,int $id);
    public function  delete(int $id);

}
