<?php
namespace App\Interfaces;

use Illuminate\Http\Request;

/**
     * Fofana Lamagnigue
     * 10/09/2022
	 */
interface UserServiceImpl{

    public function findAll();
    public function find(int $id);
    public function  save(Request $request);
    public function  update(Request $request,int $id);
    public function  delete(int $id);

}
