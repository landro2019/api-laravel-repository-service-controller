<?php
 namespace App\Service;
 use App\Interfaces\UserServiceImpl;
 use App\Repository\UserRepository;
 use App\Traits\ApiResponser;
 use Illuminate\Http\Request;

 /**
     * Fofana Lamagnigue
     * 10/09/2022
	 */
 class UserService implements UserServiceImpl{
   use ApiResponser;
 	/**
	 */
    private $userRepository;
	function __construct(UserRepository $userRepository) {
        $this->userRepository=$userRepository;
	}
	/**
	 *
	 * @return mixed
	 */
	function findAll() {
       $data= $this->userRepository->findAll();
       return $this->showData($data);
	}
	
	/**
	 *
	 * @param int $id
	 *
	 * @return mixed
	 */
	function find(int $id) {
        $data= $this->userRepository->find($id);
        return $this->showData($data);  
	}
	
	/**
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return mixed
	 */
	function save(Request $request) {
        $data = $request->validate([
         'name'=>'required|string',
         'phone'=>'required|string',
         'email'=>'required|string|unique:users',
         'password'=>'required|between:6,8|confirmed',
        ]);
        $data['password'] = bcrypt($data['password']);
        $response=  $this->userRepository->save($data);

       return $this->showData($response,201);
	}
	
	/**
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param int $id
	 *
	 * @return mixed
	 */
	function update(Request $request, int $id) {
        $data = $this->userRepository->find($id);

        if ($request->has('email')) {
          $data->email = $request->email;
        }
        if ($request->has('phone')) {
          $data->phone = $request->phone;
        }
    
        if ($request->has('name')) {
          $data->name = $request->name;
        }
        $response=   $this->userRepository->update($data,$id);
        return $this->showData($response,203);
	}
    
	
	/**
	 *
	 * @param int $id
	 *
	 * @return mixed
	 */
	function delete(int $id) {
        $response= $this->userRepository->delete($id);
        return $this->showData($response,200);
	}
}