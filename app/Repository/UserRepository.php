<?php
 namespace App\Repository;
 use App\Interfaces\UserRepositoryImpl;
use App\Models\User;
/**
     * Fofana Lamagnigue
     * 10/09/2022
	 */
 class UserRepository implements UserRepositoryImpl{

 	/**
	 */
	function __construct() {
	}
	/**
	 *
	 * @return mixed
	 */
	function findAll() {
      return User::all();
	}
	
	/**
	 *
	 * @param int $id
	 *
	 * @return mixed
	 */
	function find(int $id) {
        return User::find($id);
	}
	
	/**
	 *
	 * @param mixed $user
	 *
	 * @return mixed
	 */
	function save($user) {
        return User::create($user);
	}
	
	/**
	 *
	 * @param mixed $user
	 * @param int $id
	 *
	 * @return mixed
	 */
	function update($user, int $id) {
        $user->save();
		return User::find($id);
	}
	
	/**
	 *
	 * @param int $id
	 *
	 * @return mixed
	 */
	function delete(int $id) {
        return User::findOrFail($id)->Delete();
	}
}
