<?php

namespace App\Http\Controllers;


use Spatie\RouteAttributes\Attributes\Prefix;
use Spatie\RouteAttributes\Attributes\Get;
use Spatie\RouteAttributes\Attributes\Post;
use App\Service\UserService;
use Illuminate\Http\Request;
use Spatie\RouteAttributes\Attributes\Delete;
use Spatie\RouteAttributes\Attributes\Put;

#[Prefix('/api/v1/')]
class UserController 
{
    /**
     * Fofana Lamagnigue
     * 10/09/2022
	 */
    private $userService;
    public function __construct(UserService $userService)
    {
       $this->userService= $userService;
    }

    #[Get('users')]
    public function findAll(){
       return $this->userService->findAll();
    }

    #[Get('users/{id}')]
    public function find(int $id){
        return  $this->userService->find($id);
    }

    #[Post('users')]
    public function create(Request $request){
        return  $this->userService->save($request);
    }

    #[Put('users/{id}')]
    public function update(Request $request ,int $id){
        return  $this->userService->update($request,$id);
    }

    #[Delete('users/{id}')]
    public function delet(int $id){
        return  $this->userService->delete($id);
    }
}
